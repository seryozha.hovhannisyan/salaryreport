package com.epam.salaryReport.util;

import com.epam.salaryReport.model.EmployeeModel;
import com.epam.salaryReport.repository.entity.EmployeeEntity;

import java.util.Objects;
import java.util.Optional;

public final class Mapper {
    private Mapper() {
    }

    public static EmployeeModel mapToEmployeeModel(EmployeeEntity entity) {
        if (Objects.isNull(entity)) {
            return null;
        }
        return new EmployeeModel()
                .setId(entity.getId())
                .setFirstName(entity.getFirstName())
                .setLastName(entity.getLastName())
                .setSalary(entity.getSalary())
                .setManagerId(entity.getManagerId());
    }

    public static Optional<EmployeeEntity> mapToEmployeeEntity(String[] data) {
        if (Objects.isNull(data)) {
            return Optional.empty();
        }
        try {
            var id = Long.parseLong(data[0]);
            var firstName = data[1];
            var lastName = data[2];
            var salary = Double.valueOf(data[3]);
            var managerId = data.length == 5 ? Long.valueOf(data[4]) : null;
            return Optional.of(new EmployeeEntity(id, firstName, lastName, salary, managerId));
        } catch (NumberFormatException nfe) {
        }
        return Optional.empty();
    }
}
