package com.epam.salaryReport.repository.impl;

import com.epam.salaryReport.config.SalaryReportConfiguration;
import com.epam.salaryReport.exception.PersistenceException;
import com.epam.salaryReport.repository.EmployeeRepository;
import com.epam.salaryReport.repository.entity.EmployeeEntity;
import com.epam.salaryReport.util.Constant;
import com.epam.salaryReport.util.Mapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

public class EmployeeFileReaderRepository implements EmployeeRepository {

    private static final Logger LOGGER = Logger.getLogger(EmployeeFileReaderRepository.class.getName());
    private final File file;

    public EmployeeFileReaderRepository() {
        this(SalaryReportConfiguration.getInstance().getCsvPathname());
    }

    public EmployeeFileReaderRepository(String pathname) {
        var classLoader = getClass().getClassLoader();
        this.file = new File(classLoader.getResource(pathname).getFile());
    }

    @Override
    public Set<EmployeeEntity> getAll() {
        Set<EmployeeEntity> data = new HashSet<>();
        try {
            try (var reader = new BufferedReader(new FileReader(file))) {
                //skip CSV header
                String line = reader.readLine();
                while ((line = reader.readLine()) != null) {
                    var employee = Mapper.mapToEmployeeEntity(line.split(Constant.CSV_DELIMITER));
                    employee.ifPresent(data::add);
                }
            }
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
            throw new PersistenceException(e);
        }

        return data;
    }


}
