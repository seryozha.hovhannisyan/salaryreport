package com.epam.salaryReport.repository;

import com.epam.salaryReport.repository.entity.EmployeeEntity;

import java.util.Set;

public interface EmployeeRepository {
    Set<EmployeeEntity> getAll();
}
