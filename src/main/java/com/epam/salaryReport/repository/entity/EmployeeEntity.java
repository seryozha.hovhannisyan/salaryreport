package com.epam.salaryReport.repository.entity;

public final class EmployeeEntity {
    private final Long id;
    private final String firstName;
    private final String lastName;
    private final Double salary;
    private final Long managerId;

    public EmployeeEntity(Long id, String firstName, String lastName, Double salary, Long managerId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.managerId = managerId;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Double getSalary() {
        return salary;
    }

    public Long getManagerId() {
        return managerId;
    }
}
