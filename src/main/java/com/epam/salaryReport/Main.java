package com.epam.salaryReport;

import com.epam.salaryReport.model.OrganizationStructureTree;
import com.epam.salaryReport.repository.EmployeeRepository;
import com.epam.salaryReport.repository.impl.EmployeeFileReaderRepository;
import com.epam.salaryReport.service.StaffReportDeliveryService;
import com.epam.salaryReport.service.impl.StaffReportDeliveryServicePrintImpl;

public class Main {

    public static void main(String[] args) {
        EmployeeRepository employeeRepository = new EmployeeFileReaderRepository();
        var employees = employeeRepository.getAll();
        var tree = OrganizationStructureTree.getBuilder().buildStructure(employees).build();
        StaffReportDeliveryService deliveryService = new StaffReportDeliveryServicePrintImpl();
        deliveryService.processDelivery(tree);
    }
}