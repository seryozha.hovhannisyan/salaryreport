package com.epam.salaryReport.config;

import java.io.IOException;
import java.util.Properties;

public class SalaryReportConfiguration {

    private static SalaryReportConfiguration INSTANCE = null;

    private final Double minimumPercentageOfSalary;
    private final Double maximumPercentageOfSalary;
    private final Integer maximumLevelOfEmployeeSubordinate;
    private final String csvPathname;

    private SalaryReportConfiguration() {
        Properties properties = new Properties();

        try {
            properties.load(SalaryReportConfiguration.class.getClassLoader().getResourceAsStream("application.properties"));
            this.csvPathname = properties.get("csvPathname").toString();
            this.minimumPercentageOfSalary = Double.valueOf(properties.get("percentageOfSalary.minimum").toString());
            this.maximumPercentageOfSalary = Double.valueOf(properties.get("percentageOfSalary.maximum").toString());
            this.maximumLevelOfEmployeeSubordinate = Integer.valueOf(properties.get("maximumLevelOfEmployeeSubordinate").toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static SalaryReportConfiguration getInstance() {
        if (INSTANCE == null) {
            synchronized (SalaryReportConfiguration.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SalaryReportConfiguration();
                }
            }
        }
        return INSTANCE;
    }

    public Double getMinimumPercentageOfSalary() {
        return minimumPercentageOfSalary;
    }

    public Double getMaximumPercentageOfSalary() {
        return maximumPercentageOfSalary;
    }

    public Integer getMaximumLevelOfEmployeeSubordinate() {
        return maximumLevelOfEmployeeSubordinate;
    }

    public String getCsvPathname() {
        return csvPathname;
    }
}
