package com.epam.salaryReport.config;

import java.io.IOException;
import java.util.Properties;

public class SalaryReportMessageTemplate {

    private static SalaryReportMessageTemplate INSTANCE = null;

    private final String messageLongLevelOfEmployeeSubordinate;
    private final String messageMaximumPercentageOfSalary;
    private final String messageMinimumPercentageOfSalary;

    private SalaryReportMessageTemplate() {
        Properties properties = new Properties();

        try {
            properties.load(SalaryReportConfiguration.class.getClassLoader().getResourceAsStream("messages.properties"));
            this.messageLongLevelOfEmployeeSubordinate = properties.get("messageLongLevelOfEmployeeSubordinate").toString();
            this.messageMaximumPercentageOfSalary = properties.get("messageMaximumPercentageOfSalary").toString();
            this.messageMinimumPercentageOfSalary = properties.get("messageMinimumPercentageOfSalary").toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static SalaryReportMessageTemplate getInstance() {
        if (INSTANCE == null) {
            synchronized (SalaryReportMessageTemplate.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SalaryReportMessageTemplate();
                }
            }
        }
        return INSTANCE;
    }

    public String getMessageLongLevelOfEmployeeSubordinate() {
        return messageLongLevelOfEmployeeSubordinate;
    }

    public String getMessageMaximumPercentageOfSalary() {
        return messageMaximumPercentageOfSalary;
    }

    public String getMessageMinimumPercentageOfSalary() {
        return messageMinimumPercentageOfSalary;
    }
}
