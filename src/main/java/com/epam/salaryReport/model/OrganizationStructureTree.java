package com.epam.salaryReport.model;

import com.epam.salaryReport.repository.entity.EmployeeEntity;
import com.epam.salaryReport.util.Mapper;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public final class OrganizationStructureTree {

    private final EmployeeModel ceo;

    private OrganizationStructureTree(OrganizationStructureTreeBuilder builder) {
        this.ceo = builder.ceo;
    }

    public static OrganizationStructureTreeBuilder getBuilder() {
        return new OrganizationStructureTreeBuilder();
    }

    public EmployeeModel getCeo() {
        return ceo;
    }

    public static final class OrganizationStructureTreeBuilder {
        private EmployeeModel ceo;
        private Set<EmployeeModel> employees;

        public OrganizationStructureTree build() {
            return new OrganizationStructureTree(this);
        }

        public OrganizationStructureTreeBuilder buildStructure(Set<EmployeeEntity> employees) {
            if (Objects.isNull(employees) || employees.isEmpty()) {
                throw new IllegalArgumentException("Could not build structure as incoming employees null or empty");
            }
            var entity = employees.stream()
                    .filter(this::isCeo)
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Could not build structure as incoming employees have not seo"));

            this.ceo = Mapper.mapToEmployeeModel(entity);
            this.employees = employees.stream()
                    .filter(emp -> !isCeo(emp))
                    .map(Mapper::mapToEmployeeModel)
                    .collect(Collectors.toSet());
            buildStructure(ceo, 0);
            return this;
        }

        private void buildStructure(EmployeeModel employee, int level) {
            var subordinateDetail = prepareDetail(employee, level);
            employee.setSubordinate(subordinateDetail);
            var subs = subordinateDetail.getSubordinates();
            if (subs.isEmpty()) {
                return;
            }
            subs.forEach(e -> buildStructure(e, level + 1));
        }

        private EmployeeSubordinateDetail prepareDetail(EmployeeModel employee, int level) {
            var id = employee.getId();
            var subordinateDetailBuilder = EmployeeSubordinateDetail.getBuilder();
            var iterator = employees.iterator();
            var subordinatesTotalAmount = 0d;
            var subordinates = new HashSet<EmployeeModel>();
            while (iterator.hasNext()) {
                var subordinate = iterator.next();
                if (subordinate.getManagerId().equals(id)) {
                    subordinates.add(subordinate);
                    subordinatesTotalAmount += subordinate.getSalary();
                    iterator.remove();
                }
            }

            var subordinatesCount = subordinates.size();
            if (subordinatesCount != 0) {
                var subordinatesAverageAmount = subordinatesTotalAmount / subordinatesCount;
                subordinateDetailBuilder.setSubordinatesAverageAmount(subordinatesAverageAmount);
            }
            return subordinateDetailBuilder
                    .setSubordinates(subordinates)
                    .setManagementLevel(level)
                    .build();
        }

        private boolean isCeo(EmployeeEntity employee) {
            return employee.getManagerId() == null;
        }
    }
}
