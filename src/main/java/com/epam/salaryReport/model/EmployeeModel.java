package com.epam.salaryReport.model;

public class EmployeeModel {
    private Long id;
    private String firstName;
    private String lastName;
    private Double salary;
    private Long managerId;

    private EmployeeSubordinateDetail subordinate;

    public Long getId() {
        return id;
    }

    public EmployeeModel setId(Long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public EmployeeModel setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public EmployeeModel setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Double getSalary() {
        return salary;
    }

    public EmployeeModel setSalary(Double salary) {
        this.salary = salary;
        return this;
    }

    public EmployeeSubordinateDetail getSubordinate() {
        return subordinate;
    }

    public EmployeeModel setSubordinate(EmployeeSubordinateDetail subordinate) {
        this.subordinate = subordinate;
        return this;
    }

    public Long getManagerId() {
        return managerId;
    }

    public EmployeeModel setManagerId(Long managerId) {
        this.managerId = managerId;
        return this;
    }
}
