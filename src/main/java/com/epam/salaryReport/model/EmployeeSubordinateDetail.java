package com.epam.salaryReport.model;

import java.util.Collections;
import java.util.Set;

public final class EmployeeSubordinateDetail {
    private final Integer managementLevel;
    private final Double subordinatesAverageAmount;
    private final Set<EmployeeModel> subordinates;

    private EmployeeSubordinateDetail(EmployeeSubordinateReportBuilder builder) {
        this.managementLevel = builder.managementLevel;
        this.subordinatesAverageAmount = builder.subordinatesAverageAmount;
        this.subordinates = builder.subordinates;
    }

    public static EmployeeSubordinateReportBuilder getBuilder() {
        return new EmployeeSubordinateReportBuilder();
    }

    public Set<EmployeeModel> getSubordinates() {
        return Collections.unmodifiableSet(subordinates);
    }

    public Double getSubordinatesAverageAmount() {
        return subordinatesAverageAmount;
    }

    public Integer getManagementLevel() {
        return managementLevel;
    }

    public static final class EmployeeSubordinateReportBuilder {
        private Integer managementLevel;
        private Double subordinatesAverageAmount;
        private Set<EmployeeModel> subordinates;

        public EmployeeSubordinateDetail build() {
            return new EmployeeSubordinateDetail(this);
        }

        public EmployeeSubordinateReportBuilder setManagementLevel(Integer managementLevel) {
            this.managementLevel = managementLevel;
            return this;
        }

        public EmployeeSubordinateReportBuilder setSubordinatesAverageAmount(Double subordinatesAverageAmount) {
            this.subordinatesAverageAmount = subordinatesAverageAmount;
            return this;
        }

        public EmployeeSubordinateReportBuilder setSubordinates(Set<EmployeeModel> subordinates) {
            this.subordinates = subordinates;
            return this;
        }
    }
}
