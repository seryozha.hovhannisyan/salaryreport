package com.epam.salaryReport.service;

import com.epam.salaryReport.model.OrganizationStructureTree;

public interface StaffReportDeliveryService {
    void processDelivery(OrganizationStructureTree organizationStructureTree);
}
