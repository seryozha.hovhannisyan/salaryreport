package com.epam.salaryReport.service;

import com.epam.salaryReport.config.SalaryReportConfiguration;
import com.epam.salaryReport.config.SalaryReportMessageTemplate;
import com.epam.salaryReport.model.EmployeeModel;

import java.util.ArrayList;
import java.util.List;

/**
 * StaffReport is responsible for checking
 * is staff reportable,
 * and preparing reports
 */
public abstract class StaffReportService {
    protected final SalaryReportConfiguration configuration;
    protected final SalaryReportMessageTemplate messageTemplate;

    public StaffReportService(SalaryReportConfiguration configuration, SalaryReportMessageTemplate messageTemplate) {
        this.configuration = configuration;
        this.messageTemplate = messageTemplate;
    }

    public List<String> generateAndRetrieveReports(EmployeeModel ceo) {
        List<String> reports = new ArrayList<>();
        generateReport(ceo, reports);
        return reports;
    }

    private void generateReport(EmployeeModel root, List<String> reports) {
        var subordinateDetail = root.getSubordinate();
        if (isReportable(root)) {
            reports.add(prepareReport(root));
        }
        var subs = subordinateDetail.getSubordinates();
        for (EmployeeModel employee : subs) {
            generateReport(employee, reports);
        }
    }

    protected abstract boolean isReportable(EmployeeModel root);

    protected abstract String prepareReport(EmployeeModel root);
}
