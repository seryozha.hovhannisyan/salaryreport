package com.epam.salaryReport.service.impl;

import com.epam.salaryReport.config.SalaryReportConfiguration;
import com.epam.salaryReport.config.SalaryReportMessageTemplate;
import com.epam.salaryReport.model.EmployeeModel;
import com.epam.salaryReport.service.StaffReportService;

/**
 * ManagerReportWhoEarnLess is responsible for checking
 * is managers earn less than they should,
 * and preparing reports
 */
public class ManagerReportServiceWhoEarnLess extends StaffReportService {

    public ManagerReportServiceWhoEarnLess(SalaryReportConfiguration configuration,
                                           SalaryReportMessageTemplate messageTemplate) {
        super(configuration, messageTemplate);
    }

    @Override
    public boolean isReportable(EmployeeModel root) {
        var subordinateDetail = root.getSubordinate();
        if (subordinateDetail.getSubordinates() == null ||
                subordinateDetail.getSubordinates().isEmpty()) {
            return false;
        }
        var averageAmount = subordinateDetail.getSubordinatesAverageAmount();
        var salaryDifferences = root.getSalary() - averageAmount;
        var allowedDifferences = averageAmount * configuration.getMinimumPercentageOfSalary() / 100;
        return salaryDifferences < allowedDifferences;
    }

    @Override
    public String prepareReport(EmployeeModel root) {
        var subordinateDetail = root.getSubordinate();
        var averageAmount = subordinateDetail.getSubordinatesAverageAmount();
        var allowedDifferences = averageAmount * configuration.getMinimumPercentageOfSalary() / 100;
        var salaryDifferences = root.getSalary() - averageAmount;
        var differences = allowedDifferences - salaryDifferences;
        return String.format(messageTemplate.getMessageMinimumPercentageOfSalary(),
                root.getId(), root.getFirstName(), root.getLastName(), differences, averageAmount + allowedDifferences);
    }
}
