package com.epam.salaryReport.service.impl;

import com.epam.salaryReport.config.SalaryReportConfiguration;
import com.epam.salaryReport.config.SalaryReportMessageTemplate;
import com.epam.salaryReport.model.EmployeeModel;
import com.epam.salaryReport.service.StaffReportService;

/**
 * ManagerReportWhoEarnMore is responsible for checking
 * is managers earn more than they should,
 * and preparing reports
 */
public class ManagerReportServiceWhoEarnMore extends StaffReportService {

    public ManagerReportServiceWhoEarnMore(SalaryReportConfiguration configuration,
                                           SalaryReportMessageTemplate messageTemplate) {
        super(configuration, messageTemplate);
    }

    @Override
    public boolean isReportable(EmployeeModel root) {
        var subordinateDetail = root.getSubordinate();
        if (subordinateDetail.getSubordinates() == null ||
                subordinateDetail.getSubordinates().isEmpty()) {
            return false;
        }
        var averageAmount = subordinateDetail.getSubordinatesAverageAmount();
        var salaryDifferences = root.getSalary() - averageAmount;
        var allowedDifferences = averageAmount * configuration.getMaximumPercentageOfSalary() / 100;
        return salaryDifferences > allowedDifferences;
    }

    @Override
    public String prepareReport(EmployeeModel root) {
        var subordinateDetail = root.getSubordinate();
        var averageAmount = subordinateDetail.getSubordinatesAverageAmount();
        var allowedDifferences = averageAmount * configuration.getMaximumPercentageOfSalary() / 100;
        var salaryDifferences = root.getSalary() - averageAmount;
        var differences = salaryDifferences - allowedDifferences;
        return String.format(messageTemplate.getMessageMaximumPercentageOfSalary(),
                root.getId(), root.getFirstName(), root.getLastName(), differences, averageAmount + allowedDifferences);
    }
}
