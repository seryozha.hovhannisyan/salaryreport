package com.epam.salaryReport.service.impl;

import com.epam.salaryReport.config.SalaryReportConfiguration;
import com.epam.salaryReport.config.SalaryReportMessageTemplate;
import com.epam.salaryReport.model.EmployeeModel;
import com.epam.salaryReport.service.StaffReportService;

/**
 * EmployeesReportWhoHasLongLineReporting is responsible for checking
 * is employee reporting line long they should,
 * and preparing reports
 */
public class EmployeesReportServiceWhoHasLongLineReporting extends StaffReportService {


    public EmployeesReportServiceWhoHasLongLineReporting(SalaryReportConfiguration configuration,
                                                         SalaryReportMessageTemplate messageTemplate) {
        super(configuration, messageTemplate);
    }

    @Override
    public boolean isReportable(EmployeeModel root) {
        return root.getSubordinate().getManagementLevel() > configuration.getMaximumLevelOfEmployeeSubordinate();
    }

    @Override
    public String prepareReport(EmployeeModel root) {
        var subordinateDetail = root.getSubordinate();
        var levelDifferences = subordinateDetail.getManagementLevel() - configuration.getMaximumLevelOfEmployeeSubordinate();
        return String.format(messageTemplate.getMessageLongLevelOfEmployeeSubordinate(),
                root.getId(), root.getFirstName(), root.getLastName(), levelDifferences);
    }
}
