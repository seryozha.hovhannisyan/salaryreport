package com.epam.salaryReport.service.impl;

import com.epam.salaryReport.config.SalaryReportConfiguration;
import com.epam.salaryReport.config.SalaryReportMessageTemplate;
import com.epam.salaryReport.model.OrganizationStructureTree;
import com.epam.salaryReport.repository.impl.EmployeeFileReaderRepository;
import com.epam.salaryReport.service.StaffReportDeliveryService;
import com.epam.salaryReport.service.StaffReportService;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class StaffReportDeliveryServicePrintImpl implements StaffReportDeliveryService {
    private static final Logger LOGGER = Logger.getLogger(EmployeeFileReaderRepository.class.getName());
    private final List<StaffReportService> staffReportServices;

    public StaffReportDeliveryServicePrintImpl() {
        staffReportServices = new ArrayList<>();

        SalaryReportConfiguration configuration = SalaryReportConfiguration.getInstance();
        SalaryReportMessageTemplate messageTemplate = SalaryReportMessageTemplate.getInstance();
        staffReportServices.add(new ManagerReportServiceWhoEarnLess(configuration, messageTemplate));
        staffReportServices.add(new ManagerReportServiceWhoEarnMore(configuration, messageTemplate));
        staffReportServices.add(new EmployeesReportServiceWhoHasLongLineReporting(configuration, messageTemplate));
    }

    @Override
    public void processDelivery(OrganizationStructureTree organizationStructureTree) {
        var reportsForDelivery = new ArrayList<String>();
        staffReportServices.forEach(service -> {
            var reports = service.generateAndRetrieveReports(organizationStructureTree.getCeo());
            if (!reports.isEmpty()) {
                reportsForDelivery.addAll(reports);
            }
        });
        print(reportsForDelivery);
    }

    private void print(List<String> reports) {
        reports.forEach(LOGGER::info);
    }
}
