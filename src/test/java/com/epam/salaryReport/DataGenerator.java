package com.epam.salaryReport;


import com.epam.salaryReport.model.EmployeeModel;
import com.epam.salaryReport.repository.entity.EmployeeEntity;

import java.util.Random;
import java.util.UUID;

public final class DataGenerator {
    private static final Random RANDOM = new Random();

    private DataGenerator() {
    }

    public static String generateUUID() {
        return UUID.randomUUID().toString();
    }

    public static EmployeeEntity generateEmployeeEntity(Long managerId, Double salary) {
        return new EmployeeEntity(RANDOM.nextLong(), generateUUID(), generateUUID(), salary, managerId);
    }

    public static EmployeeEntity generateEmployeeEntity(Long managerId) {
        return new EmployeeEntity(RANDOM.nextLong(), generateUUID(), generateUUID(), RANDOM.nextDouble(), managerId);
    }

    public static EmployeeModel generateEmployeeModel() {
        return new EmployeeModel()
                .setId(RANDOM.nextLong())
                .setManagerId(RANDOM.nextLong())
                .setFirstName(generateUUID())
                .setLastName(generateUUID())
                .setSalary(RANDOM.nextDouble())
                ;

    }
}
