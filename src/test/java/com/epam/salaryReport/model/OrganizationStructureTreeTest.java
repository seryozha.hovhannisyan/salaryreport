package com.epam.salaryReport.model;

import com.epam.salaryReport.DataGenerator;
import com.epam.salaryReport.repository.entity.EmployeeEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

class OrganizationStructureTreeTest {

    @Test
    void testBuilderFromEmpty() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                OrganizationStructureTree.getBuilder().buildStructure(Set.of()));
    }

    @Test
    void testBuilderFromIllegalArgument() {
        var employee = DataGenerator.generateEmployeeEntity(6L);
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                OrganizationStructureTree.getBuilder().buildStructure(Set.of(employee)));
    }

    @Test
    void testBuilder() {
        var ceo = DataGenerator.generateEmployeeEntity(null);
        var ceoChild1 = DataGenerator.generateEmployeeEntity(ceo.getId());
        var ceoChild2 = DataGenerator.generateEmployeeEntity(ceo.getId());
        var ceoGrantChild11 = DataGenerator.generateEmployeeEntity(ceoChild1.getId());
        var ceoGrantChild111 = DataGenerator.generateEmployeeEntity(ceoGrantChild11.getId());

        var tree = OrganizationStructureTree.getBuilder().buildStructure(
                Set.of(ceoGrantChild111, ceoGrantChild11, ceo, ceoChild1, ceoChild2)
        ).build();

        var ceoTree = tree.getCeo();
        assertEquals(ceo, ceoTree);
        Assertions.assertNotNull(ceoTree.getSubordinate());

        var subordinate = ceoTree.getSubordinate();
        var expectedCeoSubordinatesAverageAmount = (ceoChild1.getSalary() + ceoChild2.getSalary()) / 2;
        Assertions.assertEquals(2, subordinate.getSubordinates().size());
        Assertions.assertEquals(0, subordinate.getManagementLevel());
        Assertions.assertEquals(expectedCeoSubordinatesAverageAmount, subordinate.getSubordinatesAverageAmount());

    }

    private void assertEquals(EmployeeEntity entity, EmployeeModel model) {
        Assertions.assertEquals(entity.getId(), model.getId());
        Assertions.assertEquals(entity.getFirstName(), model.getFirstName());
        Assertions.assertEquals(entity.getLastName(), model.getLastName());
        Assertions.assertEquals(entity.getSalary(), model.getSalary());
        Assertions.assertEquals(entity.getManagerId(), model.getManagerId());
    }
}