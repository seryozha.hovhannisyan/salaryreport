package com.epam.salaryReport.model;

import com.epam.salaryReport.DataGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

class EmployeeSubordinateDetailTest {

    @Test
    void testBuilder() {
        var model = DataGenerator.generateEmployeeModel();
        var employeeSubordinateDetail = EmployeeSubordinateDetail.getBuilder()
                .setSubordinates(Set.of(model))
                .setManagementLevel(3)
                .setSubordinatesAverageAmount(7D)
                .build();
        Assertions.assertEquals(Set.of(model), employeeSubordinateDetail.getSubordinates());
        Assertions.assertEquals(3, employeeSubordinateDetail.getManagementLevel());
        Assertions.assertEquals(7, employeeSubordinateDetail.getSubordinatesAverageAmount());
    }
}