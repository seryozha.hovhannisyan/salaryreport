package com.epam.salaryReport.util;


import com.epam.salaryReport.repository.entity.EmployeeEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MapperTest {

    @Test
    void mapToEmployeeModelFromNull() {
        Assertions.assertNull(Mapper.mapToEmployeeModel(null));
    }

    @Test
    void mapToEmployeeModel() {
        var entity = new EmployeeEntity(12L, "Joe", "Doe", 200D, 25L);
        var data = Mapper.mapToEmployeeModel(entity);
        Assertions.assertEquals(entity.getId(), data.getId());
        Assertions.assertEquals(entity.getFirstName(), data.getFirstName());
        Assertions.assertEquals(entity.getLastName(), data.getLastName());
        Assertions.assertEquals(entity.getSalary(), data.getSalary());
        Assertions.assertEquals(entity.getManagerId(), data.getManagerId());
    }

    @Test
    void mapToEmployeeEntityFromNull() {
        Assertions.assertTrue(Mapper.mapToEmployeeEntity(null).isEmpty());
    }

    @Test
    void mapToEmployeeEntityFromInvalidData() {
        var attr = new String[]{"id", "Joe", "Doe", "salary", "managerId"};
        Assertions.assertTrue(Mapper.mapToEmployeeEntity(attr).isEmpty());
    }

    @Test
    void mapToEmployeeEntity() {
        var attr = new String[]{"123", "Joe", "Doe", "60000", "300"};
        var dataOptional = Mapper.mapToEmployeeEntity(attr);

        Assertions.assertTrue(dataOptional.isPresent());
        var data = dataOptional.get();

        Assertions.assertEquals(123, data.getId());
        Assertions.assertEquals("Joe", data.getFirstName());
        Assertions.assertEquals("Doe", data.getLastName());
        Assertions.assertEquals(60000, data.getSalary());
        Assertions.assertEquals(300, data.getManagerId());
    }
}