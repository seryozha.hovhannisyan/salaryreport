package com.epam.salaryReport.repository.impl;

import com.epam.salaryReport.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EmployeeFileReaderRepositoryTest {

    @Test
    void getAll() {
        EmployeeRepository employeeRepository = new EmployeeFileReaderRepository("staff.csv");
        var employees = employeeRepository.getAll();

        Assertions.assertNotNull(employees);
        Assertions.assertEquals(2, employees.size());
    }
}