package com.epam.salaryReport.service.impl;

import com.epam.salaryReport.DataGenerator;
import com.epam.salaryReport.config.SalaryReportConfiguration;
import com.epam.salaryReport.config.SalaryReportMessageTemplate;
import com.epam.salaryReport.model.EmployeeModel;
import com.epam.salaryReport.model.OrganizationStructureTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Set;

class EmployeesReportServiceWhoHasLongLineReportingTest {


    private SalaryReportConfiguration configuration = SalaryReportConfiguration.getInstance();
    private SalaryReportMessageTemplate messageTemplate = SalaryReportMessageTemplate.getInstance();
    private EmployeesReportServiceWhoHasLongLineReporting managerReportServiceWhoEarnLess = new EmployeesReportServiceWhoHasLongLineReporting(configuration, messageTemplate);

    @Test
    void isReportable() {
        var ceo = DataGenerator.generateEmployeeEntity(null, 141D);
        var ceoChild1 = DataGenerator.generateEmployeeEntity(ceo.getId(), 101D);
        var ceoChild2 = DataGenerator.generateEmployeeEntity(ceoChild1.getId(), 102D);
        var ceoChild3 = DataGenerator.generateEmployeeEntity(ceoChild2.getId(), 103D);

        var tree = OrganizationStructureTree.getBuilder().buildStructure(
                Set.of(ceoChild1, ceo, ceoChild2, ceoChild3)
        ).build();

        var model = searchChildNodeById(ceoChild3.getId(), tree.getCeo());
        Assertions.assertTrue(managerReportServiceWhoEarnLess.isReportable(model));
    }

    @Test
    void isNotReportable() {
        var ceo = DataGenerator.generateEmployeeEntity(null, 150D);
        var ceoChild1 = DataGenerator.generateEmployeeEntity(ceo.getId(), 100D);

        var tree = OrganizationStructureTree.getBuilder().buildStructure(
                Set.of(ceo, ceoChild1)
        ).build();

        Assertions.assertFalse(managerReportServiceWhoEarnLess.isReportable(tree.getCeo()));
    }

    @Test
    void prepareReport() {
        var ceo = DataGenerator.generateEmployeeEntity(null, 141D);
        var ceoChild1 = DataGenerator.generateEmployeeEntity(ceo.getId(), 101D);
        var ceoChild2 = DataGenerator.generateEmployeeEntity(ceoChild1.getId(), 102D);
        var ceoChild3 = DataGenerator.generateEmployeeEntity(ceoChild2.getId(), 103D);

        var tree = OrganizationStructureTree.getBuilder().buildStructure(
                Set.of(ceoChild1, ceo, ceoChild2, ceoChild3)
        ).build();

        var model = searchChildNodeById(ceoChild3.getId(), tree.getCeo());
        var expectedMessage = String.format(messageTemplate.getMessageLongLevelOfEmployeeSubordinate(),
                ceoChild3.getId(), ceoChild3.getFirstName(), ceoChild3.getLastName(), 2);
        Assertions.assertEquals(expectedMessage, managerReportServiceWhoEarnLess.prepareReport(model));
    }

    @Test
    void generateAndRetrieveReports() {
        var ceo = DataGenerator.generateEmployeeEntity(null, 141D);
        var ceoChild1 = DataGenerator.generateEmployeeEntity(ceo.getId(), 101D);
        var ceoChild2 = DataGenerator.generateEmployeeEntity(ceoChild1.getId(), 102D);
        var ceoChild3 = DataGenerator.generateEmployeeEntity(ceoChild2.getId(), 103D);

        var tree = OrganizationStructureTree.getBuilder().buildStructure(
                Set.of(ceoChild1, ceo, ceoChild2, ceoChild3)
        ).build();

        var expectedMessage = String.format(messageTemplate.getMessageLongLevelOfEmployeeSubordinate(),
                ceoChild3.getId(), ceoChild3.getFirstName(), ceoChild3.getLastName(), 2);

        var messages = managerReportServiceWhoEarnLess.generateAndRetrieveReports(tree.getCeo());
        Assertions.assertEquals(2, messages.size());
        Assertions.assertTrue(messages.contains(expectedMessage));
    }

    private EmployeeModel searchChildNodeById(Long id, EmployeeModel root) {
        if (id.equals(root.getId())) {
            return root;
        }

        var subordinateDetail = root.getSubordinate();
        var subs = subordinateDetail.getSubordinates();
        for (EmployeeModel employee : subs) {
            var find = searchChildNodeById(id, employee);
            if (find != null) {
                return find;
            }
        }
        return null;
    }
}