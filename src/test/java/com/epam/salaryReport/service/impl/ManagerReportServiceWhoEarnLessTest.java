package com.epam.salaryReport.service.impl;

import com.epam.salaryReport.DataGenerator;
import com.epam.salaryReport.config.SalaryReportConfiguration;
import com.epam.salaryReport.config.SalaryReportMessageTemplate;
import com.epam.salaryReport.model.OrganizationStructureTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

class ManagerReportServiceWhoEarnLessTest {

    private SalaryReportConfiguration configuration = SalaryReportConfiguration.getInstance();
    private SalaryReportMessageTemplate messageTemplate = SalaryReportMessageTemplate.getInstance();
    private ManagerReportServiceWhoEarnLess managerReportServiceWhoEarnLess = new ManagerReportServiceWhoEarnLess(configuration, messageTemplate);

    @Test
    void isReportable() {
        var tree = prepareReportableTree();
        Assertions.assertTrue(managerReportServiceWhoEarnLess.isReportable(tree.getCeo()));
    }

    @Test
    void isNotReportable() {
        var ceo = DataGenerator.generateEmployeeEntity(null, 120D);
        var ceoChild1 = DataGenerator.generateEmployeeEntity(ceo.getId(), 100D);

        var tree = OrganizationStructureTree.getBuilder().buildStructure(
                Set.of(ceo, ceoChild1)
        ).build();

        Assertions.assertFalse(managerReportServiceWhoEarnLess.isReportable(tree.getCeo()));
    }

    @Test
    void prepareReport() {
        var tree = prepareReportableTree();
        var root = tree.getCeo();
        var expectedMessage = String.format(messageTemplate.getMessageMinimumPercentageOfSalary(),
                root.getId(), root.getFirstName(), root.getLastName(), 1d, 120d);
        Assertions.assertEquals(expectedMessage, managerReportServiceWhoEarnLess.prepareReport(root));
    }

    @Test
    void generateAndRetrieveReports() {
        var tree = prepareReportableTree();
        var root = tree.getCeo();
        var expectedMessage = String.format(messageTemplate.getMessageMinimumPercentageOfSalary(),
                root.getId(), root.getFirstName(), root.getLastName(), 1d, 120d);
        Assertions.assertEquals(List.of(expectedMessage), managerReportServiceWhoEarnLess.generateAndRetrieveReports(root));
    }

    private OrganizationStructureTree prepareReportableTree() {
        var ceo = DataGenerator.generateEmployeeEntity(null, 119D);
        var ceoChild1 = DataGenerator.generateEmployeeEntity(ceo.getId(), 100D);

        return OrganizationStructureTree.getBuilder().buildStructure(
                Set.of(ceo, ceoChild1)
        ).build();
    }
}