package com.epam.salaryReport.service.impl;

import com.epam.salaryReport.DataGenerator;
import com.epam.salaryReport.config.SalaryReportConfiguration;
import com.epam.salaryReport.config.SalaryReportMessageTemplate;
import com.epam.salaryReport.model.OrganizationStructureTree;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

class ManagerReportServiceWhoEarnMoreTest {

    private SalaryReportConfiguration configuration = SalaryReportConfiguration.getInstance();
    private SalaryReportMessageTemplate messageTemplate = SalaryReportMessageTemplate.getInstance();
    private ManagerReportServiceWhoEarnMore managerReportServiceWhoEarnLess = new ManagerReportServiceWhoEarnMore(configuration, messageTemplate);

    @Test
    void isReportable() {
        var tree = prepareReportableTree();
        Assertions.assertTrue(managerReportServiceWhoEarnLess.isReportable(tree.getCeo()));
    }

    @Test
    void isNotReportable() {
        var ceo = DataGenerator.generateEmployeeEntity(null, 150D);
        var ceoChild1 = DataGenerator.generateEmployeeEntity(ceo.getId(), 100D);

        var tree = OrganizationStructureTree.getBuilder().buildStructure(
                Set.of(ceo, ceoChild1)
        ).build();

        Assertions.assertFalse(managerReportServiceWhoEarnLess.isReportable(tree.getCeo()));
    }

    @Test
    void prepareReport() {
        var tree = prepareReportableTree();
        var root = tree.getCeo();
        var expectedMessage = String.format(messageTemplate.getMessageMaximumPercentageOfSalary(),
                root.getId(), root.getFirstName(), root.getLastName(), 1d, 150d);
        Assertions.assertEquals(expectedMessage, managerReportServiceWhoEarnLess.prepareReport(root));
    }

    @Test
    void generateAndRetrieveReports() {
        var tree = prepareReportableTree();
        var root = tree.getCeo();
        var expectedMessage = String.format(messageTemplate.getMessageMaximumPercentageOfSalary(),
                root.getId(), root.getFirstName(), root.getLastName(), 1d, 150d);
        Assertions.assertEquals(List.of(expectedMessage), managerReportServiceWhoEarnLess.generateAndRetrieveReports(root));
    }

    private OrganizationStructureTree prepareReportableTree() {
        var ceo = DataGenerator.generateEmployeeEntity(null, 151D);
        var ceoChild1 = DataGenerator.generateEmployeeEntity(ceo.getId(), 100D);

        return OrganizationStructureTree.getBuilder().buildStructure(
                Set.of(ceo, ceoChild1)
        ).build();
    }

}