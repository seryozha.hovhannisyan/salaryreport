## Technology Stack

- Application follows hexagonal architecture. Each layers used separate model.
- Application used custom tree of data structure

### Common

Application is going to be written in java.

| Tech   | Version |
|--------|---------|
| JDK    | 17      |
| Jupiter| 5.7.2   |

### Design Patterns

Application used bellow Gangs of Four (GoF) Design Patterns

- Creational: [Builder](../src/main/java/com/epam/salaryReport/model/OrganizationStructureTree.java)
- Structural: [Facade](../src/main/java/com/epam/salaryReport/repository/EmployeeRepository.java)
- Behavioral: [Chain of responsibility](../src/main/java/com/epam/salaryReport/service/StaffReportService.java)
