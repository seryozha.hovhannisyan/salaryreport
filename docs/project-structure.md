# Project structure

## Project folders

| Path                  | Description                           |
| -----------------     | ------------------------------------- |
| `docs/`               | Project documentation folder          |
| `src/main/java`       | Sources                               |
| `src/main/resources`  | Configurations/Resources              |
| `src/test/java`       | Tests                                 |
| `src/test/resources`  | Test Resources                        |

## Service and configuration

| Path                | Description                                    |
| ------------------- | ---------------------------------------------- |
| `.github/`          | GitHub settings and GitHub Actions definitions |
| `.gitignore`        | Git ignore file                                |
| `pom.xm`            | Maven pom file for project                     |

## Project Structure

Follow to the following structure for each project.

```..  code::
.
|-- com
|   |-- epam
|   |   |-- salaryReport
|   |   |   | <project>
|   |   |   |   |-- config
|   |   |   |   |-- model
|   |   |   |   |-- repository
|   |   |   |   |   |-- entity
|   |   |   |   |   |   |-- EmployeeEntity
|   |   |   |   |   |-- impl
|   |   |   |   |   |   |-- EmployeeFileReaderRepository
|   |   |   |   |   |-- EmployeeRepository
|   |   |   |   |-- service
|   |   |   |   |   |-- impl
|   |   |   |   |   |   |-- EmployeesReportServiceWhoHasLongLineReporting
|   |   |   |   |   |   |-- ManagerReportServiceWhoEarnLess
|   |   |   |   |   |   |-- ManagerReportServiceWhoEarnMore
|   |   |   |   |   |   |-- StaffReportDeliveryServicePrintImpl
|   |   |   |   |   |-- StaffReportDeliveryService
|   |   |   |   |   |-- StaffReportService
|   |   |   |   |-- util
```

| Package      | Description                                                  |
|--------------|--------------------------------------------------------------|
| `config`     | All Configurations related to project                        |
| `exception`  | All custom Runtime Exceptions                                |
| `model`      | All objects which are used to service and higher layers      |
| `repository` | Data access components, for now data come from file          |
| `entity`     | external data representation objects, ex: persistent objects |
| `service`    | All Business logic                                           |
| `util`       | Utility classes such are data mapping and constants          |

## Temporary objects, ignored in sources

| Path            | Description                                |
| ---------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| `.idea, .idea, *.ipr, *.iml, *.iws, *.ipr, */target, /target, .run`                                                                            | IntelliJ dependencies folders and files      |
| `nb-configuration.xml, /nbproject/private/, /nbbuild/, /dist/, /nbdist/, /.nb-gradle/, build/, !**/src/main/**/build/, !**/src/test/**/build/` | NetBeans dependencies folders and files      |
| `.project, .classpath, .settings/, *.launch, bin/`                                                                                             | Eclipse dependencies folders and files       |
| `.project, .classpath, .settings/, *.launch, bin/`                                                                                             | Visual Studio dependencies folders and files |
| `.DS_Store`                                                                                                                                    | OSX                                          |
| `*.swp, *.swo`                                                                                                                                 | Vim                                          |
| `*.orig, *.rej`                                                                                                                                | patch                                        |
| `.apt_generated,.springBeans, .sts4-cache`                                                                                                     | STS                                          |