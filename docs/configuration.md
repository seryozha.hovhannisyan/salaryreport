# Configuration

> The configuration is main part of Salary Report application.
> Configuration allow to setup main data to have independence on any environment.

## Application Configuration

The [application.properties](../src/main/resources/application.properties) is general configuration. The
application.properties provide settings for

- csvPathname - The path of CSV file from where repository will read data. You can change it and reference to another
  csv file
- percentageOfSalary.minimum - The minimum percentage of salary, which at least more than the average salary of its
  direct subordinates,
- percentageOfSalary.maximum - The minimum percentage of salary, which at least less than the average salary of its
  direct subordinates,
- maximumLevelOfEmployeeSubordinate - The reporting lines allowed size

## Messages Bundle

> The all messages provided in English language (Locale.US) for Salary Report application.

The Salary Report application's client messages are flexible and configurable.
For support new languages we can add new messages_hy.properties.

The client messages are located [messages.properties](../src/main/resources/messages.properties)
file.

On messages.properties provided

> Please use provided message's key's structure during adding/modifying new messages.

1. Salary Report Message Template

   This part contains message templates of Salary Report application's.

> The messages should be removed after handling Template library

## CSV data

The Salary Report application is working based on csv file. The file contains staff related data.
The [staff.csv](../src/main/resources/staff.csv) file provide default data for organization employees

- Id - Identifier of Employee
- firstName - Name of Employee
- lastName - Last name of Employee
- salary - Salary of Employee
- managerId - manager Identifier of Employee

> The all fields are mandatory. Only company ceo has no managerId