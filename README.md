# Salary Report

## Developer docs

- [Task](./docs/task.md)
- [Project structure](./docs/project-structure.md)
- [Application configuration](./docs/configuration.md)
- [Technology Stack](./docs/technologies.md)
